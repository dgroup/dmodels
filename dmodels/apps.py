from django.apps import AppConfig


class DModelsConfig(AppConfig):
    name = 'dmodels'
